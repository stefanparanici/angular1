import { Injectable } from '@angular/core';
import { Student } from './studenti/studenti.component';

@Injectable({
  providedIn: 'root'
})
export class StudentiService {
  studenti:Student[] =[
    {firstName:"Stefan",lastName:'Paranici',grade:8},
    {firstName:"Adrian",lastName:'Popovici',grade:7},
    {firstName:"Andrei",lastName:'Avramoaie',grade:4},
    {firstName:"Cosmin",lastName:'Iacob',grade:9},
    {firstName:"Vasile",lastName:'Popescu',grade:2},
    {firstName:"Valentina",lastName:'Onut',grade:3},
    {firstName:"Papusa",lastName:'Paranici',grade:10},
    {firstName:"Adriana",lastName:'Zoitanu',grade:3},
    {firstName:"Andra",lastName:'Cantemir',grade:6},
    {firstName:"Ana-Maria",lastName:'Dumitrescu',grade:8}
  ]
  addRandom(){
    let names =[
      "Stefan",
      "Adrian",
      "Andrei",
      "Cosmin",
      "Vasile",
      "Valentina",
      "Papusa",
      "Adriana",
      "Andra",
      "Ana-Maria"
   ]
   let surnames =[
    'Paranici',
    'Popovici',
    'Avramoaie',
    'Iacob',
    'Popescu',
    'Onut',
    'Paranici',
    'Zoitanu',
    'Cantemir',
    'Dumitrescu'
  ]
  let nameIndex:number = Math.floor(Math.random() * 11);
  let surnameIndex:number = Math.floor(Math.random() * 11);
  let grade:number = Math.floor(Math.random() * 11);
  this.studenti[this.studenti.length] = {firstName:names[nameIndex],lastName:surnames[surnameIndex],grade: grade}
  }
}
