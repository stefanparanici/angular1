import { Component, Input} from '@angular/core';
import { StudentiService } from '../studenti.service';

export interface Student {
  firstName: string,
  lastName: string,
  grade: number
}
@Component({
  selector: 'app-studenti',
  templateUrl: './studenti.component.html',
  styleUrls: ['./studenti.component.css']
})
export class StudentiComponent{
  StudentDB:StudentiService = new StudentiService;
  constructor() { 
  }
}
